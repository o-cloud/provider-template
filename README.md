# Provider template

This template can be used to implement a new provider based on provider library : <https://gitlab.com/o-cloud/provider-library>

A exemple of mongoDB provider based on this template can be found here : <https://gitlab.com/o-cloud/provider-mongodb-example>

Provider template provide a skeleton structure that you need to modify/implement :

* A helm chart that you need to rename and add/update values (Chart.yaml and Values.yaml)
* A skaffold.yaml associated with your helm chart that you need to update
* A config package on which you can add new global config needed for you provider
* A implement package that you need to implement to describe the logic of your provider
* A ready to run dockerfile

## Provider workflow

### Configuration

In administration panel, a admin user can deploy new provider and configure it with new scopes, descriptions and images.  
When provider is ready, it can be registered in catalog for partner subscription.  

<img src="assets/provider-Configuration.png?raw=true" width=600>

### Access creation

In user panel, a user can consult catalog and add new provider in his project.  
Next, in provider configuration, he can ask for access on provider resource.  

<img src="assets/provider-Access_creation.png?raw=true" width=1000>

### Pipeline usage

Providers in project can be added in pipeline with previous created access token or new one.  
On execution, access object is created by provider cluster and automaticaly passed on next stage of pipeline that require it.  

<img src="assets/provider-Pipeline_usage.png?raw=true" width=1000>

## Implement package

Implement package is a package that help you to implement a new provider with structured code.  

### Permissions

File *permissions.go*

In RegisterAccessPermission function, you need to register permissions that is autorized in provider configuration of scopes.  
By default AccessReadOnly is only one available for data provider type but you can register preexistant permissions or create custom permissions if needed.  

### Resources

File *resources.go*

In ListResources function, you need to list resources available in provider.  
Resources can optionaly have a category.  
Resource can be listed manualy or loaded dynamicaly based on client provider.  

### Provider client

File *provider_client.go*

Load provider client and attach it to a global variable available through geter function.  
For exemple can be a mongodb client or ftp access according to what your provider allow to access.  

### Access management

File *access_object.go*

You need to define a struct that describe what your user need to access on provider and delete it in future.  
In GenerateAccess function, create access based on scope and identities with your provider client.  
For example create user and attach roles and return their to your new struct.  

In PurgeAccess function, delete access based on connInfo received in parameter.  
For example delete user and attached roles.  

## Helm chart

You need to update Chart.yaml and values.yaml of the helm chart to define name, version, type, tls of your provider.  
Update skaffold.yaml if you want to test your provider in a test cluster with the provider name on your values.yaml.  
