# Builder Image
FROM golang:1.16 AS builder

WORKDIR /usr/src

COPY ./go.mod ./go.sum ./

RUN go mod download 

COPY . .

# Pass skaffold debug when used (by default is empty)
ARG SKAFFOLD_GO_GCFLAGS

RUN CGO_ENABLED=0 go build -gcflags="${SKAFFOLD_GO_GCFLAGS}" -o /go/bin/template-provider

# Final image
FROM scratch
COPY ./config.yaml /etc/irtsb/
COPY --from=builder /go/bin/template-provider /go/bin/template-provider

ENV GOTRACEBACK=all

ENTRYPOINT ["/go/bin/template-provider"]
