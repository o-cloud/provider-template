package config

import (
	"log"

	"github.com/spf13/viper"
	configProvider "gitlab.com/o-cloud/provider-library/config"
)

var (
	Config GeneralConfig
)

const (
	dockerConfigPath = "/etc/irtsb/"
	localConfigPath  = "./"
)

func Load() {

	viper.SetConfigType("yaml")
	viper.AddConfigPath(dockerConfigPath)
	viper.AddConfigPath(localConfigPath)

	// Find and read the config file
	viper.SetConfigName("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Unable to read config file", err)
	}

	// Decode config into ServiceConfig
	if err := viper.Unmarshal(&Config); err != nil {
		log.Fatal("unable to decode configuration", err)
	}

	// Environment variables
	viper.SetDefault("MONGO_CON_STRING", "mongodb://localhost:27017/")
	viper.BindEnv("MONGO_CON_STRING")
	Config.Provider.MongoDatabase.ConString = viper.GetString("MONGO_CON_STRING")
	viper.BindEnv("JWT_SECRET_KEY")
	Config.Provider.JwtSecretKey = viper.GetString("JWT_SECRET_KEY")
}

type GeneralConfig struct {
	Provider configProvider.ProviderConfig
	Service  ServiceConfig
}

// TODO : add you custom configuration
type ServiceConfig struct{}
