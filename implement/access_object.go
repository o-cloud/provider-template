package implement

import (
	"encoding/json"

	"gitlab.com/o-cloud/provider-library/models/access_object"
	"gitlab.com/o-cloud/provider-library/models/scopes"
)

// DO NOT TOUCH
type accessObjectController struct {
}

// DO NOT TOUCH
func RegisterAccessObjectRepository() {
	accessObjectController := &accessObjectController{}
	access_object.SetAccessObjectController(accessObjectController)
}

// TODO : Create access object struct with all informations needed to connect on provider
type access struct {
	String1 string `json:"string1"`
	String2 string `json:"string2"`
}

// TODO : Implement CreateAccess function that generate and return access object defined before
// You must not touch to function declaration
func (a *accessObjectController) GenerateAccess(scope *scopes.Scope, identities []string) access_object.ConnectionInformation {
	//client := GetProviderClient()

	// TODO : create access based on scope and identities
	// For example create user and attach roles and return their to your new struct

	access := &access{
		String1: "",
		String2: "",
	}

	return access
}

// TODO : Implement PurgeAccess function that purge access that is represented by ConnectionInformation
// You must not touch to function declaration
func (a *accessObjectController) PurgeAccess(connInfo access_object.ConnectionInformation) {
	// You need to unmarshal connInfo to read it
	var access access
	if err := json.Unmarshal(connInfo.([]byte), &access); err != nil {
		panic(err)
	}

	//client := GetProviderClient()

	// TODO : delete access based on connInfo
	// For example delete user and attached roles
}
