package implement

import "gitlab.com/o-cloud/provider-library/models/permissions"

// TODO : Register permissions that is autorized in provider configuration of scopes
//        By default AccessReadOnly is only one available for data provider type
func RegisterAccessPermission() {
	repository := permissions.GetAccessPermissionsRepository()

	// Preexistant permissions
	repository.RegisterAccessPermission(permissions.AccessReadOnly)
	//repository.RegisterAccessPermission(permissions.AccessWriteOnly)
	//repository.RegisterAccessPermission(permissions.AccessReadWrite)

	// Custom permissions if needed
	//CustomAccessPermission permissions.AccessPermission = "Custom"
	//repository.RegisterAccessPermission(CustomAccessPermission)
}
