module gitlab.com/o-cloud/provider

go 1.16

require (
	github.com/spf13/viper v1.8.1
	gitlab.com/o-cloud/provider-library v0.0.0-20220127141541-f27bbf169832
)
